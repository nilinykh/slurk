.. _slurk_quickstart:

=========================================
Slurk: quickstart tutorial
=========================================


Requirements
~~~~~~~~~~~~

In order to start the game, you need Python 3.6+.
Python can be installed from `the official website <https://www.python.org/downloads/>`_
or with `Anaconda <https://anaconda.org/anaconda/python>`_. You also need to install
*pip*. Additionally, *virtualenv* package can be installed if desired.

You also need to have at least 2 different web browsers on your machine to be able to
test the game [1]_.
In this tutorial we will use Firefox and Google Chrome.

Setup
~~~~~

Unix-based systems

- setting up virtual environment (optional)

  - ``virtualenv -p /usr/bin/python3.6 my_virtualenv``
  - ``source my_virtualenv/bin/activate``

- configuring the application

  - ``pip install -r requirements.txt``

- cleaning up the repository

  - ``rm -rf botsi.db``
  - make a copy of ``config.template.ini``, name it ``config.ini``

  - adjust ``host``, ``port`` and ``ssl`` in ``config.ini``

Starting the Slurk, activating game bots
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Run one of the commands below

- ``python chat.py``
- ``gunicorn chat:app -b 127.0.0.1:5000 -k geventwebsocket.gunicorn.workers.GeventWebSocketWorker``

Access the chat login browser window by visiting ``127.0.0.1:5000``
Both bots (ConciergeBot and MultiRoom Bot) need tokens to be authenticated in the game:

- visit ``127.0.0.1:5000/token``
- bots' token parameters: source - anything, room - Waiting Room, task - None, reusable - yes
- change directory to ``server/sample_bots``
- ``python pairup_bot.py -c 127.0.0.1 -p 5000 [token without square brackets]``

Now your application is running along with the ConciergeBot who waits for the players in the Waiting Room.
The next step would be to generate token with the same parameters described above, but use it for
the MultiRoom bot:

- follow first two steps to generate another bot token
- ``python multibot.py -c 127.0.0.1 -p 5000 [token without square brackets]``

Congratulations! Now you have two bots living in your game environment.

Playing the Slurk
~~~~~~~~~~~~~~~~~~

You can start the game by taking one of the ways described below

------------
First method
------------

You will need to generate tokens for the players. Follow the same instructions as above,
but choose different parameters for players' tokens

- players' token parameters: source - anything, room - Waiting Room, task - meetup, reusable - no
- open ``127.0.0.1:5000`` in Firefox and in Google Chrome
- write any name in the name field
- replace `None` with the player's token in the token field
- click ``enter chatroom``

-------------
Second method
-------------

You do not need to generate tokens for the players, just run the script that does this:

- ``python meetup_login.py``

Now you should see both players in the game room. Enjoy the game!

-----------------------------------------------

.. [1] Since we test our game locally on one machine, using the same web browser for two different players will result in their merge.
