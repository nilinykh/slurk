
##################
Slurk
##################


.. toctree::
   :hidden:
   :maxdepth: 4
   :numbered:

   slurk_quickstart
   slurk_overview
   slurk_login
   slurk_chat
   slurk_commands
   slurk_events
   slurk_deployment

   meetup_main
   meetup_deployment

Welcome!

**Slurk** is the multi-level project, that provides tools for
smooth and convenient dialogue data collection. Despite handling
multi-user interactions in various chat environments simultaneously,
Slurk's mechanism can be upgraded with multimodal
inventory, including images, videos or sounds. Additionally, Slurk has necessary
appliances for crowd-sourcing with Amazon Mechanical Turk.

For more information, please refer to :ref:`slurk_overview`

If you want to read about specific project, that uses Slurk, please refer to
:ref:`meetup_main`.

Enjoy slurking!
